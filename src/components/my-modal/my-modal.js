import React, { Component } from 'react';
import './my-modal.css';

const MyModal =(props)=> {
        const { showModal, onCloseModal, action} = props;
        const display = showModal ? "block" : "none";
        return (
            <div className="modal" tabIndex="-1" role="dialog" style={{ display }}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">{props.title}</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={() => onCloseModal()}>
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                        <p>{props.question}</p>
                    </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary" onClick={action}>{props.confirmTitle}</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={() => onCloseModal()}>Close</button>
                    </div>
                    </div>
                </div>
            </div>
        )
}
export default MyModal