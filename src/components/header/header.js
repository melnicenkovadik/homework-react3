import React from 'react';
import { NavLink } from 'react-router-dom';

const Header = () => {
    return (
        <header>
          <nav className='navbar navbar-expand-lg navbar-dark bg-dark' >
            <img alt='LOGO' width="30" height="30" className="d-inline-block align-top" />
            <ul className='navbar-nav'>
                <li className="nav-item nav-link">
                <NavLink className='nav-link'  to="/">Home</NavLink>
              </li>

              <li className="nav-item nav-link">
                <NavLink className='nav-link' to="/chosen">Chosen</NavLink>
              </li>

              <li className="nav-item nav-link float-right">
                <NavLink className='nav-link' to="/cart">
                  <span className="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</NavLink>
              </li>
            </ul>
        </nav>
        </header>
    )
}

export default Header;