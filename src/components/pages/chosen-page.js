import React from 'react';
import Header from '../header';
import ProductList from '../product-list';

const ChosenPage = ({ data, onChooseItem}) => {
    return (
        <React.Fragment>
            <Header />
            <ProductList data={data} onChooseItem={onChooseItem}/>
        </React.Fragment>
    )
}

export default ChosenPage;