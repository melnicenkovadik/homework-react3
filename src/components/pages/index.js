import ChosenPage from './chosen-page';
import CartPage from './cart-page';
import HomePage from './home-page';

export {
    ChosenPage, CartPage, HomePage
}