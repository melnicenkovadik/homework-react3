import React, { Component } from 'react';
import Header from '../header';
import ProductList from '../product-list';

const CartPage =(props)=>{
        const { data, onDeleteFromCart, onChooseItem } = props;
        return (
            <React.Fragment>
                <Header />
                <ProductList data={data} onDeleteFromCart={onDeleteFromCart} onChooseItem={onChooseItem}/>
            </React.Fragment>
        )

}

export default CartPage;