import React, { Component } from 'react'
import ProductItem from '../product-item';
import './product-list.css';

 const ProductList = ({ onAddToCart, onChooseItem, onDeleteFromCart, data }) => {
  const renderItems = arr => {
    return arr.map(item => {
      const { id, ...itemProps } = item;
      return (
        <li key={id} className="product-list-item-group">
          <ProductItem
            data={itemProps}
            onAddToCart={() => onAddToCart(item)}
            onChooseItem={() => onChooseItem(id)}
            onDeleteFromCart={() => onDeleteFromCart(item)}
            displayDeleteButton={(onDeleteFromCart) ? true : false}
            displayAddButton={(onAddToCart) ? true : false}/>
        </li>
      );
    });
  }
  return (
    <ul className="product-list">
      {renderItems(data)}
    </ul>
  )
}
export default ProductList
//
// export default class ProductList extends Component{
//   renderItems(arr){
//     return arr.map((item) => {
//       const { id, ...itemProps } = item;
//       return (
//         <li key={id} className="product-list-item-group">
//           <ProductItem
//             data={itemProps}
//             onAddToCart={() => this.props.onAddToCart(item)}
//             onChooseItem={() => this.props.onChooseItem(id)}
//             onDeleteFromCart={() => this.props.onDeleteFromCart(item)}
//             displayDeleteButton={(this.props.onDeleteFromCart) ? true : false}
//             displayAddButton={(this.props.onAddToCart) ? true : false}/>
//         </li>
//       );
//     });
//   }
//
//   render(){
//     const { data } = this.props;
//     return (
//       <ul className="product-list">
//           {this.renderItems(data)}
//       </ul>
//     )
//   }
// }