import React, { useEffect,useState } from 'react';
import MyModal from '../my-modal'
import './app.css';
import axios from 'axios';
import { CartPage, ChosenPage, HomePage } from '../pages';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
const getProducts = () => {
  return axios.get('products.json');
}

const writeToLocalStorage = (key, value) => {
  const keyValue = localStorage.getItem(key);
  if(keyValue && keyValue.length !== 0){
    const localStorageArray = JSON.parse(keyValue);
    if(!localStorageArray.find((item) => item === value)){
      localStorageArray.push(value);
      localStorage.setItem(key, JSON.stringify(localStorageArray));
    }
    console.log(localStorageArray);
  } else {
    localStorage.setItem(key, JSON.stringify([value]));
  }
}

const deleteFromLocalStorage = (key, valueToDelete) => {
  const keyValue = localStorage.getItem(key);
  if(keyValue){
    const localStorageArray = JSON.parse(keyValue);
    const index = localStorageArray.indexOf(valueToDelete);
    if (index > -1) {
      localStorageArray.splice(index, 1);
      localStorage.setItem(key, JSON.stringify(localStorageArray));
    }
  }
}

const getDataFromLocalStorage = (key) => {

  const value = localStorage.getItem(key);
  if (!value){
    return []
  }
  if(value && value.length !== 0){
    return JSON.parse(value);
  }
}

const filteredData = (allData, idsArray) => {
  return allData.filter((item)=> {
    if(idsArray.includes(item.id)){
      return item;
    }
  })
}

 let App = () => {
  const [data, setDate] = useState(null);
  const [modal, setModal] = useState(false);
  const [modalActoin, setModalAction] = useState(null);
  const [cartItem, setCartItem] = useState(null);
  const [cart, setCart] = useState([]);
  const [chosen, setChosen] = useState([]);


  useEffect( ()=>{
    getProducts().then((response) => {
      let data = response.data;
      const chosenProducts = JSON.parse(localStorage.getItem("chosen"));
      //Set chosen products from localStorage
      if(chosenProducts){
        data = data.map((item) => {
          if(chosenProducts.includes(item.id)){
            item.isChosen = true;
          }
          return item;
        });
      }
      return data;
    })
    .then((result) => {
      setDate(result);
    })
      console.log('mount')
  },[console.log('unmount')]
  )

let toogleChooseItem = (id) => {
    setDate( (()=>{
      const idx = data.findIndex((el) => el.id === id);

      const oldItem = data[idx];
      const newItem = { ...oldItem, ["isChosen"]: !oldItem["isChosen"] };

      if(newItem.isChosen === false){
        deleteFromLocalStorage("chosen", newItem.id);
      }
      else{
        writeToLocalStorage("chosen", newItem.id);
      }

      const newArray =  [...data.slice(0, idx), newItem, ...data.slice(idx + 1)];
      return  newArray
    })())
  }


 let onCloseModal = () => {
   setModal((() => {
      const currentModalState = (modal) ? false : true
      return currentModalState
    })())
  }


 let onAddToCartConfirm = () => {
    if(cartItem){
      const { id } = cartItem;
      writeToLocalStorage("cart", id);
      onCloseModal();
    }
  }

 let onDeleteFromCartConfirm = () => {
    if(cartItem){
      const { id } = cartItem;
      deleteFromLocalStorage("cart", id);
      onCloseModal();
    }
  }

 let onAddToCart = (item) => {
      setCartItem( item)
      setModal(true)
      setModalAction ('add')
  }

let onDeleteFromCart = (item) => {
      setCartItem (item)
      setModal (true)
      setModalAction ('delete')
  }

  let switchModalActions = (action) => {
    switch(action){
      case 'add':
        return { 
          title: "Додавання в корзину", 
          question: "Ви впевнені що хочете додати товар в корзину?", 
          confirmTitle: "Додати в корзину",
          action: onAddToCartConfirm
        }
      case 'delete':
        return { 
          title: "Видалення з корзини", 
          question: "Ви впевнені що хочете видалити товар з корзини?", 
          confirmTitle: "Видалити з корзини",
          action: onDeleteFromCartConfirm
        }
      default:
        return { title: "", question: "", action: onCloseModal}
    }
  }

    if(!data)
      return <div>No data yet</div>
    
    const {title, question, confirmTitle, action} = switchModalActions(modalActoin);

    return (
      <div className="App">
        <Router>
          <MyModal 
            title={title}
            question={question}
            showModal={modal} 
            action={action} 
            confirmTitle={confirmTitle}
            onCloseModal={onCloseModal}/>

          <Switch>
            <Route path="/cart">
              <CartPage data={filteredData(data, getDataFromLocalStorage('cart'))} onDeleteFromCart={onDeleteFromCart} onChooseItem={toogleChooseItem}/>
            </Route>
            <Route path="/chosen">
              <ChosenPage data={filteredData(data, getDataFromLocalStorage('chosen'))} onAddToCart={onAddToCart} onChooseItem={toogleChooseItem} />
            </Route>
            <Route path="/">
              <HomePage data={data} onAddToCart={ onAddToCart } onChooseItem={ toogleChooseItem } />
            </Route>
          </Switch>
        </Router>
      </div>
    )

}
export default App
